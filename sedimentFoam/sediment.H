//Correcting the velocity to the u_j-ws term in the advection term
USed = U - ws;

//USed.correctBoundaryConditions();       //Not used in this case (need to check what are the implications)

//Compute the sediment face fluxes on the cell faces
phiSed = (fvc::interpolate(USed) & mesh.Sf());

//Adding passive scalar
fvScalarMatrix sedEqn
(
      fvm::ddt(sed)                 //dC/dt
    + fvm::div(phiSed,sed)          //d{(u_j-ws*delta_i,3)C}/dx_j
    - fvm::laplacian(gammaSed,sed)  //kappa*d^2(C)/dx_j^2
);

//Under-relax the equation if specified by the user
sedEqn.relax();

//Solve the equation and update the scalar field
sedEqn.solve();

//Print info to the screen
Info << "Maximum sed: " << max(sed).value() << " and Minimum sed: " << min(sed).value() << endl;

